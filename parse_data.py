import glob
import pandas as pd
import json

# Load file-tag info
dataFolder = 'data/LDC2018E45_AIDA_Scenario_1_Seedling_Annotation_V5.0/data/'
files = glob.glob(dataFolder+'T10?/T10?_ent_mentions.tab')

df = (pd.read_csv(f,sep='\t') for f in files)
df_cat = pd.concat(df, ignore_index=True)

img_gt_list = df_cat[(df_cat.textoffset_startchar.isnull())]

text_gt_list = df_cat[(df_cat.textoffset_startchar.notnull())]

# Load UID-file info
p_c1 = pd.read_csv('data/LDC2018E01_AIDA_Seedling_Corpus_V1/docs/parent_children.tab',sep='\t')
p_c2 = pd.read_csv('data/LDC2018E52_AIDA_Scenario_1_Seedling_Corpus_Part_2/docs/parent_children.tab',sep='\t')
parent_child = pd.concat((p_c1,p_c2), ignore_index=True)

# Extract dict for img-tag and text-tag

# Dict of file:tag
tmp_dict_ = {}
for index,row in img_gt_list.iterrows():
    provenance = row[3]
    # If there is a translation, use it instead of Russian text
    if str(row[7])!='nan':
        text = (row[7],row[10])
    else:
        text = (row[6],row[10])
    if provenance not in tmp_dict_:
        tmp_dict_[provenance] = [text]
    else:
        tmp_dict_[provenance].append(text)
# Dict of UID:file   
# Video is exclued
img_gt_dict = {}
img_count = 0
for k in list(tmp_dict_.keys()):
    # parent_id_list = parent_child[parent_child['child_file'].str.startswith(k)]['parent_uid']
    parent_id_list = parent_child[(parent_child['child_file'].str.startswith(k)) & (parent_child['dtyp'] =='img')]['parent_uid']
    if len(parent_id_list) == 0:
        continue
    else:
        img_count += len(parent_id_list)
        parent_id = parent_id_list.values[0]
        if parent_id not in img_gt_dict:
            img_gt_dict[parent_id] = [(k,tmp_dict_[k])]
        else:
            img_gt_dict[parent_id].append((k,tmp_dict_[k]))
# print('There are {} tags among {} images'.format(img_gt_list.shape[0],len(list(tmp_dict_.keys()))))
print('There are {} tags among {} images'.format(img_gt_list.shape[0],img_count))
print('These images belongs to {} parent_uid'.format(len(list(img_gt_dict.keys()))))





with open('img_gt_dict.json', 'w') as outfile:
    json.dump(img_gt_dict, outfile)
    print('Dict of image groundtruth dumped')

tmp_dict_ = {}
for index,row in text_gt_list.iterrows():
    provenance = row[3]
    if str(row[7])!='nan':
        text = (row[7],row[10])
    else:
        text = (row[6],row[10])
    if provenance not in tmp_dict_:
        tmp_dict_[provenance] = [text]
    else:
        tmp_dict_[provenance].append(text)

text_gt_dict = {}
for k in list(tmp_dict_.keys()):
    parent_id_list = parent_child[parent_child['child_file'].str.startswith(k)]['parent_uid']
    if len(parent_id_list) == 0:
        continue
    else:
        parent_id = parent_id_list.values[0]
        if parent_id not in text_gt_dict:
            text_gt_dict[parent_id] = [(k,tmp_dict_[k])]
        else:
            text_gt_dict[parent_id].append((k,tmp_dict_[k]))
print('There are {} tags among {} text files'.format(text_gt_list.shape[0],len(list(tmp_dict_.keys()))))
print('These text files belongs to {} parent_uid'.format(len(list(text_gt_dict.keys()))))
with open('text_gt_dict.json', 'w') as outfile:
    json.dump(text_gt_dict, outfile)
    print('Dict of text groundtruth dumped')
