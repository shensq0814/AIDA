import json

# Load data from disk
f_pr = open('img_pr_dict.json','r')
f_gt = open('img_gt_dict.json','r')
img_pr_dict = json.load(f_pr)
img_gt_dict = json.load(f_gt)


gt_count = 0
pr_count = 0
gt_img = 0
pr_img = 0
current_count = 0
common_keys = list(img_pr_dict.keys() & img_gt_dict.keys())
print("There are {} common parent_UID for ground truth & valid detection".format(len(common_keys)))
for k in common_keys:
    print(img_gt_dict[k],'\n')
    print(img_pr_dict[k])
    print('\n\n\n')

    
    gt = img_gt_dict[k]
    gt_img += len(img_gt_dict[k])

    pr = img_pr_dict[k]
    pr_img += len(img_pr_dict[k])

    # print('{:10}{:10}'.format(len(gt),len(pr)))

    for p in pr:
        pr_count += len(p[1])
    for g in gt:
        gt_count += len(g[1])
        g_kbid_ = [x[1] for x in g[1]]
        
        for p in pr:
            p_kbid_ = [x[1] for x in p[1]]
            if (g[0] != p[0]):
                continue
            else:
                for g_id_ in g_kbid_:
                    for p_id_ in p_kbid_:
                        if g_id_==p_id_:
                            current_count += 1
#                             print(g)
#                             print(p)
#                             print('\n')
print("There are {} ground truth tags in {} images".format(gt_count,gt_img))
print("There are {} predicted tags in {} images".format(pr_count,pr_img))
print("Number of current matches: {}".format(current_count))
print("Precision = {}/{} = ".format(current_count,pr_count),(current_count/pr_count))
print("Recall = {}/{} = ".format(current_count,gt_count), (current_count/gt_count))