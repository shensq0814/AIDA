import json
import gensim
import numpy as np

def find_max(a):
    out = []
    is_touched = np.zeros(a.shape)

    r,c = a.shape
    # Sort in descending order on flatten array
    rank = np.argsort(a,axis=None)[::-1]
    # print(rank)s
    
    for i in rank:
        pos = (i//c,i%c)
        if is_touched[pos]:
            continue
        else:
            is_touched[pos[0],:]=1
            is_touched[:,pos[1]]=1
            out.append(pos)
    return out


# Load data from json files
f_img = open('img_dict.json','r')
f_text_gt = open('text_gt_dict.json','r')
f_img_gt = open('img_gt_dict.json','r')
img_dict = json.load(f_img)
img_gt_dict = json.load(f_img_gt)
text_gt_dict = json.load(f_text_gt)

# Find common keys
common_keys = list(text_gt_dict.keys() & img_gt_dict.keys() & img_dict.keys())
print("There are {} parent_uid has text&image annotation and valid detection.".format(len(common_keys)))
print(common_keys)

# Load pre-trained model
model = gensim.models.KeyedVectors.load_word2vec_format('./data/GoogleNews-vectors-negative300.bin', binary=True)  
word_vectors = model.wv
del model

img_total = 0
text_total = 0
img_total_unfil = 0
text_total_unfil = 0
img2text_currect = 0
text2img_currect = 0


img_pr_dict = {}
for k in common_keys:
    print('Parent_ID: '+k+'\n')
    img_files = img_dict[k]
    text_files = text_gt_dict[k]

  
    # Text embedding is done once for a PARENT_ID
    text_embedding = []
    text_filt = []
    
    text_tags = text_files[0][1]
    print("Text tag for {}:\n".format(text_files[0][0]),text_tags,'\n')
    for t_tag in text_tags:
        t_vector = [0]*300
        w_count = 0
        is_vocab = False

        for word in t_tag[0].replace('-',' ').split(): 
            if word in word_vectors.vocab:

                is_vocab = True
#                 print("Embedding {}".format(word))
                t_vector += word_vectors[word]
                w_count+=1
        if is_vocab:
            text_filt.append(t_tag)
            t_vector /= w_count
            text_embedding.append(t_vector)
    
    if len(text_filt):
        text_total += len(text_filt)

        text_np = np.asarray(text_embedding)

        text_norm = np.linalg.norm(text_np, axis=1)

        text_np = text_np/text_norm[:,np.newaxis]



    
    
    # Embedding for each image in the same PARENT_ID
    for img in img_files:
        img_tags = img[1]
        print("Image tag for {}:\n".format(img[0]),img_tags)
        
        img_embedding = []
        img_filt = []
    
        # counting
        img_total_unfil += len(img_tags)
        text_total_unfil += len(text_tags)

        for i_tag in img_tags:
#             print("current_tag: ",i_tag)
            i_vector = [0] * 300

            # keep record of the words in a tag
            w_count = 0 
            is_vocab = False

            # Split Phrase into words
            for word in i_tag.split():
                if word in word_vectors.vocab:
#                     print("Embedding -- {}".format(word))
                    is_vocab = True
                    w_count += 1
                    i_vector += word_vectors[word]

            # If i_tag is a valid tag, store its vector
            if is_vocab:
#                 print("Valid tag -- {}".format(i_tag))
                img_filt.append((i_tag,''))
                i_vector /= w_count
                img_embedding.append(i_vector)
    
    
    
    

        if (len(img_filt)) and (len(text_filt)):
            img_total += len(img_filt)

            img_np = np.asarray(img_embedding)
            
            img_norm = np.linalg.norm(img_np, axis=1)

            img_np = img_np/img_norm[:,np.newaxis]

            cosine_sim = np.dot(img_np,text_np.T)
            print('\n',"Similarity matrix shape: ",cosine_sim.shape,'\n')

            out = find_max(cosine_sim)
            print("Best matching pairs: ",out)
            print('\n'+'{:40}{:40}{:30}'.format('IMAGE TAG','TEXT TAG','KB_ID'))
            info_concat = []
            
            for o in out:
            
                print('{:40}{:40}{:30}'.format(img_filt[o[0]][0],text_filt[o[1]][0],text_filt[o[1]][1]))
                img_filt[o[0]] = (img_filt[o[0]][0],text_filt[o[1]][1])

                info_concat.append(img_filt[o[0]])
            print('\n')
            
            if k not in img_pr_dict:
                img_pr_dict[k] = [(img[0][:-4],info_concat)]
            else:
                img_pr_dict[k].append((img[0][:-4],info_concat))
            print('\n')


# Store result in json
with open('img_pr_dict.json', 'w') as outfile:
    json.dump(img_pr_dict, outfile)