# AIDA T1
## Info of dataset
### Number of images
|          | jpg       | png  | gif | total
| ------------- |-------------:| -----:|-----:|---:|
| Part1 | 955|23|5|983|
|Part2 |1835|70|1|1906|
|All part| 2790| 93| 6 | 2889|
### Images used for object detection
2767 images are used in object detection. They are all jpg images with RGB channels.

### Number of tags in annotation
|          | number       |
| ------------- |-------------:|
| Image tag| 972|
| Text tag| 2989|
## Parse original data into dict
Download data files and put them into folder according to the python file. 

The result dicts is in form of:
* Parent_ID
  * File_ID
  * Annotation
    * Tag
    * KB_ID

### Get dict from ground truth
```
python parse_data.py
```
It reads annatation for entities from topic T101-T107 from *T10?_ent_mention.tab*, and the uid-file relationship from *parent_children.tab*

Two dictionaries dumped by json will be generated into the current folder. 
- img_gt_dict.json
- text_gt_dict.json
  

| Term          | Number        | Other  |
| ------------- |:-------------:| -----:|
| img_tag           | 972           | Tag extract from *justification*. Including tags from video|
| image         |    332       | 463 if video is included  |
| img parent_uid    | 130           |    207 if video is included |
| text_tag           | 2989           | Translated text is used |
| text files         | 475           |   |
| text parent_uid    | 179           | Each UID correspond to only 1 text file  |

### Get dict from object detection result
```
python parse_detected.py
```
The uid-file relationship from *parent_children.tab* is also used here. Besides, object dection result in *.h5* format is needed.

A dictionary will be generated into the current folder.
- img_dict.json

| Term | Number|Other|
|------|:-----:|----:|
|images after object detection| 2767|
|valid object| 776 | > 0.3|
|valid images| 254|valid object >= 1 |
|valid img parent_uid| 149 | |

## Find best match in image tags and text tags
Only using *Document* with **text & image annotation** and has **valid image** from detection result.

### Word embedding
Install *gensim* before running.

Download word vector from [GoogleNews-vectors-negative300.bin](https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit).

**Loading model may take few minutes, depending on the computer config**

```
python find_match.py
```
Result will be store to json file in the current folder.
- img_pr_dict.json

### Sample output:
Words not exist in word vector, mostly Russian, are ignored and do **not** go into the similarity matrix.

For word pharases, such as *Russian millitary*, the embedding vector is the average of all its component words. **Space** and **Dash** are used for parsing.

Greedy algorithm is used to find the best match pair.
Each text tag is only matched to one image tag.
```
Parent_ID: IC00163GT

Text tag for HC000T6HU:
 [['Дебальцево', 'E0255'], ['Russian millitary', 'NIL336.974102'], ['artillery', 'NIL80.974102']]

Image tag for HC000SVSK.jpg:
 ['Truck', 'Person']

 Similarity matrix shape:  (2, 2)

Best matching pairs:  [(0, 1), (1, 0)]

IMAGE TAG                               TEXT TAG                                KB_ID
Truck                                   artillery                               NIL80.974102
Person                                  Russian millitary                       NIL336.974102



Image tag for IC00163HC.jpg:
 ['Sculpture', 'Person']

 Similarity matrix shape:  (2, 2)

Best matching pairs:  [(0, 1), (1, 0)]

IMAGE TAG                               TEXT TAG                                KB_ID
Sculpture                               artillery                               NIL80.974102
Person                                  Russian millitary                       NIL336.974102
```

## Evaluation
Only using *Document* with **image ground truth tag** and **valid prediction from embedding**. Video is excluded.

The difference between predicted & ground truth image is because some images has no valid object in it.
```
evaluate.py
```

| Term          | Number        | Other  |
| ------------- |:-------------:| -----:|
| valid document | 11           | |
| ground truth img         | 50           |   |
| ground truth tag    | 67           |     |
| predicted img         | 26          | |
| predicted tag       | 55           |   |
| current match   | 4          | |

>Precision = 4/55 =  0.07272727272727272 <br>
>Recall = 4/67 =  0.05970149253731343

## Result with type
Prdiction
Precision = 51/187 =  0.2727272727272727 <br>
Recall = 51/83 =  0.6144578313253012

Upper Bound
Recall = 58/83 =  0.6987951807228916
