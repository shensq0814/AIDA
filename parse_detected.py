import h5py
import numpy as np
import json 
import pandas as pd

filename1 = 'data/tag_bounding/results_part1.h5'
filename2 = 'data/tag_bounding/results_part2.h5'
f1 = h5py.File(filename1,'r')
f2 = h5py.File(filename2,'r')

detection_threshold = 0.3

key_list = list(f1.keys())
score_list = list(f1[key_list[3]]) + list(f2[key_list[3]])
class_list = list(f1[key_list[1]]) + list(f2[key_list[1]])
file_list = list(f1[key_list[2]]) + list(f2[key_list[2]])


score_list = np.asarray(score_list,dtype=np.float32)
class_list = np.asarray(class_list,dtype=str)
file_list = np.asarray(file_list,dtype=str)

detect_dict_ = {}
obj_num = 0
img_num = 0
for f,s,c in zip(file_list,score_list,class_list):
    l = list(c[s>0.3])
    if len(l)>0:
        obj_num+=len(l)
        img_num += 1
        detect_dict_[f] = l
print('The data has {} images\' object detection result'.format(file_list.shape[0]))
print('There are {} objects detected among {} images'.format(obj_num,img_num))

# Load UID-file info
p_c1 = pd.read_csv('data/LDC2018E01_AIDA_Seedling_Corpus_V1/docs/parent_children.tab',sep='\t')
p_c2 = pd.read_csv('data/LDC2018E52_AIDA_Scenario_1_Seedling_Corpus_Part_2/docs/parent_children.tab',sep='\t')
parent_child = pd.concat((p_c1,p_c2), ignore_index=True)

img_dict = {}
for k in list(detect_dict_.keys()):
    parent_id_list = parent_child[parent_child['child_file'].str.startswith(k)]['parent_uid']
    if len(parent_id_list) == 0:
        continue
    else:
        parent_id = parent_id_list.values[0]
        if parent_id not in img_dict:
            img_dict[parent_id] = [(k,detect_dict_[k])]
        else:
            img_dict[parent_id].append((k,detect_dict_[k]))

print('The images have valid objects belong to {} parent_uid'.format(len(list(img_dict.keys()))))
with open('img_dict.json', 'w') as outfile:
    json.dump(img_dict, outfile)
    print('Dict of image runned object detection dumped')