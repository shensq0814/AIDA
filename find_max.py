import numpy as np

a = np.array([[ 0.10221094,  0.02774689,  0.02384684,  0.03490132,  0.03490132, 0.06865324,  0.05170799,  0.10837225,  0.16471381],
       [ 0.06764577, -0.01629899,  0.02052857,  0.02288486,  0.02288486, 0.0524357 ,  0.00403207,  0.06674057,  0.0747538 ],
       [ 0.02262736, -0.06369004, -0.01474065,  0.02601456,  0.02601456,-0.00029837,  0.07219835,  0.05426505,  0.15690427]])
# (3,9)
# Find the largest one for each row, no duplicate col

def find_max(a):
    out = []
    is_touched = np.zeros(a.shape)

    r,c = a.shape
    # Sort in descending order on flatten array
    rank = np.argsort(a,axis=None)[::-1]
    print(rank)
    
    for i in rank:
        pos = (i//c,i%c)
        if is_touched[pos]:
            continue
        else:
            is_touched[pos[0],:]=1
            is_touched[:,pos[1]]=1
            out.append(pos)
    return out

find_max(a)